# Fedora Design TikTok GitLab

This repository is used to collect and manage potential video ideas for our TikTok page. We use GitLab's issue tracking system to store and organize our ideas, allowing us to easily prioritize, assign, and track progress on each idea.

## How to submit a video idea

To submit a video idea, please follow these steps:

    1. Click on the "Issues" tab at the top of the repository.
    2. Click the "New Issue" button.
    3. Fill out the form with your idea, providing as much detail as possible.
    4. Include any links or references that may be relevant to your idea.
    5. Submit your issue.

Once your idea is submitted, it will be reviewed by the repository maintainers. If the idea is accepted, it will be assigned an acceptance label. If the idea is not accepted, it may be closed with an explanation of why it was not a good fit.

## How to view and track progress on video ideas

To view all video ideas, click on the "Issues" tab at the top of the repository. From here, you can view all open and closed issues, filter by label or assignee, and sort by priority or date.

To track progress on an individual idea, click on the issue to view its details. From here, you can view its priority, assignee, labels, and comments. You can also add comments, change the priority or assignee, or close the issue if it has been completed.

Once the video  been approved, it will be scheduled to post. The ticket will be uploaded with the TikTok link and then closed out.


## Contribution guidelines

We welcome contributions to this repository from anyone who has a potential video idea. To ensure that your idea is considered, please follow these guidelines:

*     Use the issue template provided when submitting an idea.
*     Provide as much detail as possible, including why the idea is relevant, what topics it covers, and any additional resources or materials that could be helpful.
*     Be respectful and constructive in all comments and discussions.

## Licence

This repository is licensed under the MIT Licence. For more information, see LICENSE.md.